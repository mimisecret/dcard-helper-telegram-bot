import logging
import random

from telegram import Update, InputMediaPhoto
from telegram.ext import Updater, Dispatcher, CallbackContext, CommandHandler

from dcard_api import Dcard, Post

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

dcard = Dcard()


def main():
    bot_token = get_token_from("bot_token.txt")

    logger.info("Starting updater...")
    logger.debug(f"Bot token: {bot_token}")
    updater = Updater(bot_token)

    dispatcher: Dispatcher = updater.dispatcher

    dispatcher.add_handler(CommandHandler("start", handle_start))
    dispatcher.add_handler(CommandHandler("help", handle_help))
    dispatcher.add_handler(CommandHandler("need", handle_need))
    dispatcher.add_handler(CommandHandler("pop", handle_pop))
    dispatcher.add_handler(CommandHandler("board_alias", handle_board_alias))

    updater.start_polling()
    logger.info("Updater is starting polling updates")
    updater.idle()


def get_token_from(file_path: str) -> str:
    with open(file_path, "r") as f:
        return f.readline().strip()


def handle_start(update: Update, context: CallbackContext) -> None:
    update.message.reply_text("Start")


def handle_help(update: Update, context: CallbackContext) -> None:
    update.message.reply_text("Help")


def handle_need(update: Update, context: CallbackContext) -> None:
    posts = dcard.get_posts("sex")
    index = random.randint(0, len(posts) - 1)
    post = posts[index]
    if len(post.mediaMeta) == 0:
        update.message.reply_text("No need, try again!")
        return

    media_list = []
    for media in post.media:
        url = media.url
        media_list.append(InputMediaPhoto(url))
    update.message.reply_media_group(media_list)


def handle_pop(update: Update, context: CallbackContext) -> None:
    posts: [Post] = dcard.get_posts("sex", popular=True)
    index = random.randint(0, len(posts))
    msg = generate_message_from_post(posts[index])
    update.message.reply_text(msg)


def handle_board_alias(update: Update, context: CallbackContext) -> None:
    update.message.reply_text("Board alias")


def generate_message_from_post(post: Post) -> str:
    msg = f"{post.title}\n{post.excerpt}\n\n" \
          f"https://www.dcard.tw/f/{post.forumAlias}/p/{post.id}"
    return msg


if __name__ == '__main__':
    main()
