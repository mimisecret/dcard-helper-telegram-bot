import unittest

from dcard_api import Dcard


class DcardApiTest(unittest.TestCase):

    def __init__(self, methodName: str = ...) -> None:
        super().__init__(methodName)
        self.dcard = Dcard()

    def test_read_board_list(self):
        boards = self.dcard.get_boards()
        board_count = len(boards)
        print(f"Boards count: {board_count}")
        board = boards[0]
        print(board)
        print(board.id)
        self.assertTrue(board_count > 0)

    def test_read_post_list(self):
        board_name = "sex"
        post_list = self.dcard.get_posts(board_name, limit=5, popular=True)
        post_count = len(post_list)
        print(f"Posts count: {post_count}")
        post = post_list[0]
        print(post)
        print(post.id)
        self.assertEqual(5, len(post_list))

    def test_read_all_post_list(self):
        post_list = self.dcard.get_posts(limit=5)
        post_count = len(post_list)
        print(f"Posts count: {post_count}")
        self.assertEqual(5, post_count)


if __name__ == '__main__':
    unittest.main()
