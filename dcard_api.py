from typing import List

import requests

from dcard_model import Board, Post


class Dcard:
    def __init__(self):
        self.host = "https://www.dcard.tw/service/api/v2"

    def get_boards(self) -> List[Board]:
        url = f"{self.host}/forums"
        resp = requests.get(url)
        boards = Board.schema().loads(resp.content, many=True)
        return boards

    def get_posts(self, board_name=None, limit=30, before: int = None, popular=False) -> List[Post]:
        if board_name is None:
            url = f"{self.host}/posts"
        else:
            url = f"{self.host}/forums/{board_name}/posts"

        if popular:
            popular = "true"
        else:
            popular = "false"

        params = {
            "limit": limit,
            "before": before,
            "popular": popular
        }

        resp = requests.get(url, params)
        posts = Post.schema().loads(resp.content, many=True)
        return posts
