from dataclasses import dataclass
from typing import List, Dict

from dataclasses_json import dataclass_json, Undefined


@dataclass_json(undefined=Undefined.EXCLUDE)
@dataclass()
class Board:
    @dataclass_json(undefined=Undefined.EXCLUDE)
    @dataclass()
    class PostThumbnail:
        size: str = None

    @dataclass_json(undefined=Undefined.EXCLUDE)
    @dataclass()
    class IpCountryCondition:
        exclude: List[str] = None
        include: List[str] = None

    @dataclass_json(undefined=Undefined.EXCLUDE)
    @dataclass()
    class HeroImage:
        url: str = None
        type: str = None
        width: int = None
        height: int = None

    @dataclass_json(undefined=Undefined.EXCLUDE)
    @dataclass()
    class Logo:
        url: str = None
        type: str = None
        width: int = None
        height: int = None

    @dataclass_json(undefined=Undefined.EXCLUDE)
    @dataclass()
    class PostCount:
        last30Days: int = None

    id: str = None
    alias: str = None
    name: str = None
    description: str = None
    subscriptionCount: int = None
    subscribed: bool = False
    read: bool = False
    createdAt: str = None
    updatedAt: str = None
    canPost: bool = False
    ignorePost: bool = False
    invisible: bool = False
    isSchool: bool = False
    fullyAnonymous: bool = False
    canUseNickname: bool = False
    postThumbnail: PostThumbnail = None
    shouldCategorized: bool = False
    shouldPostCategorized: bool = False
    hasPostCategories: bool = False
    titlePlaceholder: str = None
    postTitlePlaceholder: str = None
    ipCountryCondition: IpCountryCondition = None
    subcategories: List[str] = None
    topics: List[str] = None
    nsfw: bool = False
    mediaThreshold: Dict = None
    limitCountries: List[str] = None
    limitStage: int = None
    availableLayouts: List[str] = None
    heroImage: HeroImage = None
    logo: Logo = None
    postCount: PostCount = None
    favorite: bool = False
    enablePrivateMessage: bool = False


@dataclass_json(undefined=Undefined.EXCLUDE)
@dataclass()
class Post:
    @dataclass_json(undefined=Undefined.EXCLUDE)
    @dataclass()
    class Meta:
        disableAdSense: bool = None

    @dataclass_json(undefined=Undefined.EXCLUDE)
    @dataclass()
    class MediaMeta:
        id: str = None
        url: str = None
        normalizedUrl: str = None
        thumbnail: str = None
        type: str = None
        tags: List[str] = None
        createdAt: str = None
        updateAt: str = None
        width: int = None
        height: int = None

    @dataclass_json(undefined=Undefined.EXCLUDE)
    @dataclass()
    class Reaction:
        id: str = None
        count: int = None

    @dataclass_json(undefined=Undefined.EXCLUDE)
    @dataclass()
    class Media:
        url: str = None

    id: int = None
    title: str = None
    excerpt: str = None
    anonymousSchool: bool = None
    anonymousDepartment: bool = None
    pinned: bool = None
    forumId: str = None
    replyId: int = None
    createdAt: str = None
    updatedAt: str = None
    commentCount: int = None
    likeCount: int = None
    withNickname: bool = None
    tags: List[str] = None
    topics: List[str] = None
    meta: Meta = None
    forumName: str = None
    forumAlias: str = None
    nsfw: bool = None
    gender: str = None
    school: str = None
    department: str = None
    replyTitle: str = None
    mediaMeta: List[MediaMeta] = None
    reactions: List[Reaction] = None
    hidden: bool = None
    customStyle: str = None
    isSuspiciousAccount: bool = None
    isModerator: bool = None
    layout: str = None
    pinnedType: str = None
    pinnedPriority: int = None
    spoilerAlert: bool = None
    categories: List[str] = None
    totalCommentCount: int = None
    withImages: bool = None
    withVideos: bool = None
    media: List[Media] = None
    reportReasonText: str = None
    excerptComments: List[str] = None
    postAvatar: str = None
    activityAvatar: str = None
    verifiedBadge: bool = None
    memberType: str = None
