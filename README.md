# Dcard Helper Telegram Bot

[Dcard Helper Bot](http://t.me/dcard_helper_bot)

## Runtime Enviroment

- Python 3.9.5

## How to Run

1. Use `pip install -r requirements.txt` to install required packages.
2. Create `bot_token.txt` at project directory, and put your bot token in this file.
3. Run `python main.py` to start your bot.